﻿namespace tamagochi
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.mainPanel = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.thealthPanel = new System.Windows.Forms.TableLayoutPanel();
            this.healthLabel = new System.Windows.Forms.Label();
            this.healthPanel = new System.Windows.Forms.TableLayoutPanel();
            this.boredPanel = new System.Windows.Forms.TableLayoutPanel();
            this.playPanel = new System.Windows.Forms.TableLayoutPanel();
            this.boredLabel = new System.Windows.Forms.Label();
            this.buttonPanel = new System.Windows.Forms.TableLayoutPanel();
            this.foodButton = new System.Windows.Forms.Button();
            this.poopButton = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.CatAndPoopPanel = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.poopImage = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.progressBarHealth = new System.Windows.Forms.ProgressBar();
            this.progressBarBored = new System.Windows.Forms.ProgressBar();
            this.mainPanel.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.thealthPanel.SuspendLayout();
            this.healthPanel.SuspendLayout();
            this.boredPanel.SuspendLayout();
            this.playPanel.SuspendLayout();
            this.buttonPanel.SuspendLayout();
            this.CatAndPoopPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.poopImage)).BeginInit();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.Color.Transparent;
            this.mainPanel.ColumnCount = 1;
            this.mainPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.mainPanel.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.mainPanel.Controls.Add(this.buttonPanel, 0, 2);
            this.mainPanel.Controls.Add(this.CatAndPoopPanel, 0, 1);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.RowCount = 3;
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13.74322F));
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 86.25678F));
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.mainPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.mainPanel.Size = new System.Drawing.Size(1182, 574);
            this.mainPanel.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.3856F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.6144F));
            this.tableLayoutPanel1.Controls.Add(this.thealthPanel, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.boredPanel, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1176, 60);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // thealthPanel
            // 
            this.thealthPanel.ColumnCount = 2;
            this.thealthPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.thealthPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.thealthPanel.Controls.Add(this.healthLabel, 0, 0);
            this.thealthPanel.Controls.Add(this.healthPanel, 1, 0);
            this.thealthPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.thealthPanel.Location = new System.Drawing.Point(3, 3);
            this.thealthPanel.Name = "thealthPanel";
            this.thealthPanel.RowCount = 1;
            this.thealthPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.thealthPanel.Size = new System.Drawing.Size(586, 24);
            this.thealthPanel.TabIndex = 0;
            // 
            // healthLabel
            // 
            this.healthLabel.AutoSize = true;
            this.healthLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.healthLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.healthLabel.Location = new System.Drawing.Point(0, 0);
            this.healthLabel.Margin = new System.Windows.Forms.Padding(0);
            this.healthLabel.Name = "healthLabel";
            this.healthLabel.Size = new System.Drawing.Size(70, 24);
            this.healthLabel.TabIndex = 0;
            this.healthLabel.Text = "Голод";
            // 
            // healthPanel
            // 
            this.healthPanel.ColumnCount = 2;
            this.healthPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.healthPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.healthPanel.Controls.Add(this.progressBarHealth, 0, 0);
            this.healthPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.healthPanel.Location = new System.Drawing.Point(87, 0);
            this.healthPanel.Margin = new System.Windows.Forms.Padding(0);
            this.healthPanel.Name = "healthPanel";
            this.healthPanel.RowCount = 1;
            this.healthPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.healthPanel.Size = new System.Drawing.Size(499, 24);
            this.healthPanel.TabIndex = 2;
            // 
            // boredPanel
            // 
            this.boredPanel.ColumnCount = 2;
            this.boredPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.boredPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85F));
            this.boredPanel.Controls.Add(this.playPanel, 0, 0);
            this.boredPanel.Controls.Add(this.boredLabel, 0, 0);
            this.boredPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.boredPanel.Location = new System.Drawing.Point(3, 33);
            this.boredPanel.Name = "boredPanel";
            this.boredPanel.RowCount = 1;
            this.boredPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.boredPanel.Size = new System.Drawing.Size(586, 24);
            this.boredPanel.TabIndex = 1;
            // 
            // playPanel
            // 
            this.playPanel.ColumnCount = 2;
            this.playPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 0F));
            this.playPanel.Controls.Add(this.progressBarBored, 0, 0);
            this.playPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.playPanel.Location = new System.Drawing.Point(87, 0);
            this.playPanel.Margin = new System.Windows.Forms.Padding(0);
            this.playPanel.Name = "playPanel";
            this.playPanel.RowCount = 1;
            this.playPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.playPanel.Size = new System.Drawing.Size(499, 24);
            this.playPanel.TabIndex = 3;
            // 
            // boredLabel
            // 
            this.boredLabel.AutoSize = true;
            this.boredLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.boredLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.boredLabel.Location = new System.Drawing.Point(0, 0);
            this.boredLabel.Margin = new System.Windows.Forms.Padding(0);
            this.boredLabel.Name = "boredLabel";
            this.boredLabel.Size = new System.Drawing.Size(86, 24);
            this.boredLabel.TabIndex = 1;
            this.boredLabel.Text = "Радість";
            // 
            // buttonPanel
            // 
            this.buttonPanel.ColumnCount = 3;
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.buttonPanel.Controls.Add(this.foodButton, 0, 0);
            this.buttonPanel.Controls.Add(this.poopButton, 2, 0);
            this.buttonPanel.Controls.Add(this.playButton, 1, 0);
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPanel.Location = new System.Drawing.Point(0, 482);
            this.buttonPanel.Margin = new System.Windows.Forms.Padding(0);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.RowCount = 1;
            this.buttonPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.buttonPanel.Size = new System.Drawing.Size(1182, 92);
            this.buttonPanel.TabIndex = 2;
            // 
            // foodButton
            // 
            this.foodButton.BackgroundImage = global::tamagochi.Properties.Resources.catfood;
            this.foodButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.foodButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.foodButton.Dock = System.Windows.Forms.DockStyle.Right;
            this.foodButton.FlatAppearance.BorderSize = 0;
            this.foodButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.foodButton.ForeColor = System.Drawing.Color.Black;
            this.foodButton.Location = new System.Drawing.Point(304, 0);
            this.foodButton.Margin = new System.Windows.Forms.Padding(0);
            this.foodButton.Name = "foodButton";
            this.foodButton.Size = new System.Drawing.Size(89, 92);
            this.foodButton.TabIndex = 0;
            this.foodButton.UseVisualStyleBackColor = true;
            this.foodButton.Click += new System.EventHandler(this.foodButton_Click);
            // 
            // poopButton
            // 
            this.poopButton.BackgroundImage = global::tamagochi.Properties.Resources.cleaning;
            this.poopButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.poopButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.poopButton.Dock = System.Windows.Forms.DockStyle.Left;
            this.poopButton.FlatAppearance.BorderSize = 0;
            this.poopButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.poopButton.ForeColor = System.Drawing.Color.Black;
            this.poopButton.Location = new System.Drawing.Point(786, 0);
            this.poopButton.Margin = new System.Windows.Forms.Padding(0);
            this.poopButton.Name = "poopButton";
            this.poopButton.Size = new System.Drawing.Size(89, 92);
            this.poopButton.TabIndex = 2;
            this.poopButton.UseVisualStyleBackColor = true;
            this.poopButton.Click += new System.EventHandler(this.poopButton_Click);
            // 
            // playButton
            // 
            this.playButton.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.playButton.BackgroundImage = global::tamagochi.Properties.Resources.playCAt;
            this.playButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.playButton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.playButton.FlatAppearance.BorderSize = 0;
            this.playButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.playButton.ForeColor = System.Drawing.Color.Black;
            this.playButton.Location = new System.Drawing.Point(537, 0);
            this.playButton.Margin = new System.Windows.Forms.Padding(0);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(105, 92);
            this.playButton.TabIndex = 1;
            this.playButton.UseVisualStyleBackColor = true;
            this.playButton.Click += new System.EventHandler(this.playButton_Click);
            // 
            // CatAndPoopPanel
            // 
            this.CatAndPoopPanel.ColumnCount = 2;
            this.CatAndPoopPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.55087F));
            this.CatAndPoopPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.44913F));
            this.CatAndPoopPanel.Controls.Add(this.pictureBox1, 1, 0);
            this.CatAndPoopPanel.Controls.Add(this.poopImage, 0, 0);
            this.CatAndPoopPanel.Location = new System.Drawing.Point(0, 66);
            this.CatAndPoopPanel.Margin = new System.Windows.Forms.Padding(0);
            this.CatAndPoopPanel.Name = "CatAndPoopPanel";
            this.CatAndPoopPanel.RowCount = 1;
            this.CatAndPoopPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.CatAndPoopPanel.Size = new System.Drawing.Size(891, 390);
            this.CatAndPoopPanel.TabIndex = 3;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBox1.Image = global::tamagochi.Properties.Resources.cat_6005844_960_720;
            this.pictureBox1.Location = new System.Drawing.Point(480, 3);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(408, 384);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // poopImage
            // 
            this.poopImage.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.poopImage.Image = global::tamagochi.Properties.Resources.poop;
            this.poopImage.Location = new System.Drawing.Point(171, 271);
            this.poopImage.Name = "poopImage";
            this.poopImage.Size = new System.Drawing.Size(135, 116);
            this.poopImage.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.poopImage.TabIndex = 2;
            this.poopImage.TabStop = false;
            this.poopImage.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 2000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // progressBarHealth
            // 
            this.progressBarHealth.AccessibleRole = System.Windows.Forms.AccessibleRole.StatusBar;
            this.progressBarHealth.BackColor = System.Drawing.Color.Red;
            this.progressBarHealth.Cursor = System.Windows.Forms.Cursors.Default;
            this.progressBarHealth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarHealth.ForeColor = System.Drawing.Color.Red;
            this.progressBarHealth.Location = new System.Drawing.Point(0, 0);
            this.progressBarHealth.Margin = new System.Windows.Forms.Padding(0);
            this.progressBarHealth.Name = "progressBarHealth";
            this.progressBarHealth.Size = new System.Drawing.Size(499, 24);
            this.progressBarHealth.TabIndex = 0;
            this.progressBarHealth.Value = 100;
            // 
            // progressBarBored
            // 
            this.progressBarBored.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.progressBarBored.Dock = System.Windows.Forms.DockStyle.Fill;
            this.progressBarBored.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.progressBarBored.Location = new System.Drawing.Point(0, 0);
            this.progressBarBored.Margin = new System.Windows.Forms.Padding(0);
            this.progressBarBored.Name = "progressBarBored";
            this.progressBarBored.Size = new System.Drawing.Size(499, 24);
            this.progressBarBored.TabIndex = 0;
            this.progressBarBored.Value = 100;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::tamagochi.Properties.Resources.room;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1182, 574);
            this.Controls.Add(this.mainPanel);
            this.Name = "Form1";
            this.Text = "Form1";
            this.mainPanel.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.thealthPanel.ResumeLayout(false);
            this.thealthPanel.PerformLayout();
            this.healthPanel.ResumeLayout(false);
            this.boredPanel.ResumeLayout(false);
            this.boredPanel.PerformLayout();
            this.playPanel.ResumeLayout(false);
            this.buttonPanel.ResumeLayout(false);
            this.CatAndPoopPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.poopImage)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel mainPanel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel thealthPanel;
        private System.Windows.Forms.Label healthLabel;
        private System.Windows.Forms.TableLayoutPanel boredPanel;
        private System.Windows.Forms.Label boredLabel;
        private System.Windows.Forms.TableLayoutPanel buttonPanel;
        private System.Windows.Forms.Button foodButton;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button poopButton;
        private System.Windows.Forms.TableLayoutPanel healthPanel;
        private System.Windows.Forms.TableLayoutPanel playPanel;
        private System.Windows.Forms.TableLayoutPanel CatAndPoopPanel;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox poopImage;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.ProgressBar progressBarHealth;
        private System.Windows.Forms.ProgressBar progressBarBored;
    }
}

